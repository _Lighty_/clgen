/// <reference types='jquery' />
var isRainbowOnly = false;
function rgbToHex(R, G, B) {
  return toHex(R) + toHex(G) + toHex(B);
}
function toHex(n) {
  n = parseInt(n, 10);
  if (isNaN(n)) return '00';
  n = Math.max(0, Math.min(n, 255));
  return (
    '0123456789ABCDEF'.charAt((n - (n % 16)) / 16) +
    '0123456789ABCDEF'.charAt(n % 16)
  );
}
function forceUpdate() {
  var textArray = [];
  if (isRainbowOnly) {
    let val = $('#rainbow-only-input').val();
    textArray.push({ txt: val, col: '', rainbow: true });
  } else {
    $('.textinput').each(function() {
      let self = $(this);
      let val = self.find('.txtinput').val();
      if (!val) return;
      let clr = self
        .find('.colinput')
        .val()
        .substr(1);
      let rainbow = self.find('.rainbowswitch').prop('checked');
      textArray.push({ txt: val, col: clr, rainbow: rainbow });
    });
  }
  if (textArray.length) {
    let result = '';
    let result_html = '';
    let wasPreviousColored = false;
    let isFirst = $('#fix-initial').prop('checked');
    if ($('#apply-colon').prop('checked')) {
      if (isFirst) {
        result += '\u0001'
        isFirst = false;
      }
      result += ':  ';
      result_html += '<span style=\'color:white;\'>:&nbsp;&nbsp;</span>';
    }
    textArray.forEach(function(a) {
      if (a.rainbow) {
        let i,
          s,
          p,
          spaces = 0,
          spaces_ = 0,
          ccol;
        for (var b = 0; b < a.txt.length; b++) {
          if (a.txt.charAt(b) == ' ') spaces += 1;
        }
        for (var b = 0; b < a.txt.length; b++) {
          if (a.txt.charAt(b) == ' ') {
            result += ' ';
            result_html += '&nbsp;';
            spaces_ += 1;
            continue;
          }
          i = (b - spaces_) / (a.txt.length - spaces);
          s = 1 / 6;
          p = (i % s) / s;
          if (i >= s * 0) ccol = rgbToHex(255, 255 * p, 0);
          if (i >= s * 1) ccol = rgbToHex(255 * (1 - p), 255, 0);
          if (i >= s * 2) ccol = rgbToHex(0, 255, 255 * p);
          if (i >= s * 3) ccol = rgbToHex(0, 255 * (1 - p), 255);
          if (i >= s * 4) ccol = rgbToHex(255 * p, 0, 255);
          if (i >= s * 5) ccol = rgbToHex(255, 0, 255 * (1 - p));
          result += '\u0007' + ccol + a.txt.charAt(b);
          result_html += '<span style=\'color:#' + ccol + ';\'>' + a.txt.charAt(b) + '</span>';
        }
        wasPreviousColored = true;
      } else {
        if (a.col && a.col !== 'FFFFFF') {
          result += '\u0007' + a.col + a.txt;
          result_html +=
            '<span style=\'color:#' + a.col + ';\'>' + a.txt + '</span>';
          wasPreviousColored = true;
        } else {
          if (wasPreviousColored || isFirst) result += '\u0001' + a.txt;
          else result += a.txt;
          result_html += '<span style=\'color:white;\'>' + a.txt + '</span>';
          wasPreviousColored = false;
        }
      }
      isFirst = false;
    });
    $('#result').html(result_html);
    $('#output').val(result);
    $('#charcount')
      .text(
        result.length +
          '/129 chars' +
          (result.length > 129
            ? ' Warning: text may not fully show in chat'
            : '')
      )
      .removeAttr('class')
      .addClass(result.length > 129 ? 'is-bad' : 'is-good');
  }
}
function createNew() {
  let newid = Math.floor(Math.random() * 1000);
  let r00t = $('<div>').addClass('textinput');
  let root = $('<li>').addClass('mdl-list__item no-padding');
  let root2 = $('<div>').addClass('mdl-grid no-padding');
  $('<div>')
    .addClass('mdl-cell mdl-cell--6-col')
    .append(
      $('<form>').append(
        $('<div>')
          .addClass(
            'mdl-textfield mdl-js-textfield mdl-textfield--floating-label'
          )
          .css({ width: '100%' })
          .append([
            $('<input>')
              .addClass('mdl-textfield__input txtinput')
              .attr({ type: 'text', id: 'txtin-' + newid })
              .on('change paste keyup', function() {
                forceUpdate();
              }),
            $('<label>')
              .addClass('mdl-textfield__label')
              .attr('for', 'txtin-' + newid)
              .text('Text')
          ])
      )
    )
    .appendTo(root2);
  $('<div>')
    .addClass('mdl-cell mdl-cell--2-col')
    .append(
      $('<form>').append(
        $('<div>')
          .addClass(
            'mdl-textfield mdl-js-textfield mdl-textfield--floating-label'
          )
          .append([
            $('<input>')
              .addClass('mdl-textfield__input colinput')
              .attr({ type: 'text', id: 'clrin-' + newid })
              .colorPicker({ noAlpha: true })
              .on('change paste keyup', function() {
                forceUpdate();
                if (!this.value) $(this).removeAttr('style');
              }),
            $('<label>')
              .addClass('mdl-textfield__label')
              .attr('for', 'clrin-' + newid)
              .text('Color')
          ])
      )
    )
    .appendTo(root2);
  $('<div>')
    .addClass('mdl-cell mdl-cell--2-col center-content')
    .append(
      $('<label>')
        .addClass('mdl-switch mdl-js-switch')
        .attr('for', 'switch' + newid)
        .append([
          $('<input>')
            .addClass('mdl-switch__input rainbowswitch')
            .attr({ type: 'checkbox', id: 'switch' + newid })
            .on('change', function() {
              forceUpdate();
            }),
          $('<span>')
            .addClass('mdl-switch__label')
            .text('Rainbow')
        ])
    )
    .appendTo(root2);
  $('<div>')
    .addClass('mdl-cell mdl-cell--2-col center-content')
    .append(
      $('<button>')
        .addClass(
          'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent'
        )
        .text('delete')
        .click(function() {
          let self = this.parentElement.parentElement.parentElement
            .parentElement;
          $(self).slideUp(400, function() {
            self.remove();
            forceUpdate();
          });
        })
    )
    .appendTo(root2);
  root2.appendTo(root);
  root.appendTo(r00t);
  r00t.hide();
  componentHandler.upgradeElements(r00t.get()[0]);
  r00t.appendTo($('#text-list'));
  r00t.slideDown();
}
$('#addbutton').click(function() {
  createNew();
});
createNew();
$('#rainbow-only').click(function() {
  $('#text-list').slideToggle();
  $('#rainbow-only-form').slideToggle();
  $('#addbutton').slideToggle();
  isRainbowOnly = !isRainbowOnly;
  if (isRainbowOnly) {
    let result = '';
    $('.textinput').each(function() {
      let self = $(this);
      let val = self.find('.txtinput').val();
      if (val) result += val;
    });
    let roi = $('#rainbow-only-input');
    roi
      .parent()
      .get()[0]
      .MaterialTextfield.change(result);
    roi.change();
  } else forceUpdate();
});
$('#rainbow-only-input').on('change paste keyup', function() {
  forceUpdate();
});
$('.txtinput')
  .parent()
  .get()[0]
  .MaterialTextfield.change('Hello world!');
$('.rainbowswitch')
  .parent()
  .click();
$('#text-list').sortable({
  axis: 'y',
  animation: 200,
  stop: function(ev, ui) {
    forceUpdate();
  }
});
$('#fix-initial').change(function() {
  forceUpdate();
});
$('#apply-colon').change(function() {
  forceUpdate();
});
